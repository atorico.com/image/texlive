from ubuntu:xenial

run apt-get update \
    && apt-get install -y \
    software-properties-common build-essential

run LC_ALL=C.UTF-8 add-apt-repository ppa:rmescandon/yq \
    && apt-get update \
    && apt-get install -y \
    time \
    dlocate \
    xzdec \
    wget \
    git \
    sudo \
    && apt-get clean 

run wget https://github.com/scottkosty/install-tl-ubuntu/raw/master/install-tl-ubuntu
run chmod +x ./install-tl-ubuntu
run ./install-tl-ubuntu -q https://ctan.space-pro.be/tex-archive/systems/texlive/tlnet/
